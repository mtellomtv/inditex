package com.amaris.inditex.service;

import com.amaris.inditex.dao.PriceDao;
import com.amaris.inditex.exception.NotFoundPriceException;
import com.amaris.inditex.model.Brand;
import com.amaris.inditex.model.Price;
import com.amaris.inditex.model.Product;
import com.amaris.inditex.model.Rate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PriceServiceImplTest {

    @InjectMocks
    PriceServiceImpl priceService;

    @Mock
    PriceDao priceDao;

    @Test
    void getCurrentPrice() {

        List<Price> prices = new ArrayList<>();
        Price price1 = Price.builder()
                .brand(Brand.builder().id(1l).name("ZARA").build())
                .currency("EUR")
                .id(3l)
                .product(Product.builder().id(1234l).name("product1").build())
                .priority(1)
                .build();
        prices.add(price1);
        Price price2 = Price.builder()
                .brand(Brand.builder().id(1l).name("ZARA").build())
                .currency("EUR")
                .id(5l)
                .product(Product.builder().id(1134l).name("product1").build())
                .priority(0)
                .build();
        prices.add(price2);
        Rate rate = Rate.builder()
                .brandId(1l)
                .productId(1134l)
                .applicationDate(LocalDateTime.now())
                .build();
        when(priceDao.getCurrentPrice(rate)).thenReturn(prices);

        Price priceReturned = priceService.getCurrentPrice(rate);

        assertThat(priceReturned).isEqualTo(price1);

    }

    @Test
    void getEmptyCurrentPrice() {
        List<Price> prices = new ArrayList<>();

        Rate rate = Rate.builder()
                .brandId(1l)
                .productId(1134l)
                .applicationDate(LocalDateTime.now())
                .build();
        when(priceDao.getCurrentPrice(rate)).thenReturn(prices);

        assertThrows(NotFoundPriceException.class, () -> priceService.getCurrentPrice(rate));
    }

    @Test
    void getNullCurrentPrice() {
        List<Price> prices = null;

        Rate rate = Rate.builder()
                .brandId(1l)
                .productId(1134l)
                .applicationDate(LocalDateTime.now())
                .build();
        when(priceDao.getCurrentPrice(rate)).thenReturn(prices);

        assertThrows(NotFoundPriceException.class, () -> priceService.getCurrentPrice(rate));
    }
}