package com.amaris.inditex.batch.job;

import com.amaris.inditex.dao.entity.BrandEntity;
import com.amaris.inditex.dao.entity.PriceEntity;
import com.amaris.inditex.dao.entity.ProductEntity;
import com.amaris.inditex.dao.repository.PriceRepository;
import com.amaris.inditex.testutil.dao.repository.BrandRepository;
import com.amaris.inditex.testutil.dao.repository.ProductRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@SpringBatchTest
class PriceCSVToDataBaseTest {

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    @Autowired
    private JobRepositoryTestUtils jobRepositoryTestUtils;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    @Qualifier("jobPriceCSVToDataBase")
    private Job jobPriceCSVToDataBase;

    @Autowired
    private PriceRepository priceRepository;

    @AfterEach
    public void cleanUp() {
        jobRepositoryTestUtils.removeJobExecutions();
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db/init_database.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:db/clean.sql")
    })
    public void dataIsAddedToDataBase() throws Exception {

        List<BrandEntity> brandBeforeExecution = brandRepository.findAll();
        List<ProductEntity> productBeforeExecution = productRepository.findAll();

        JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncherTestUtils.launchJob();

        List<PriceEntity> priceList = priceRepository.findAll();

        List<PriceEntity> expectedData = new ArrayList<>();

        BrandEntity expectedBrand = new BrandEntity();
        expectedBrand.setId(1l);
        ProductEntity expectedProduct = new ProductEntity();
        expectedProduct.setId(35455l);

        expectedData.add(new PriceEntity(1l, expectedBrand, LocalDateTime.of(2020, 6, 14, 0, 0, 0)
                , LocalDateTime.of(2020, 12, 31, 23, 59, 59), expectedProduct, 0,
                BigDecimal.valueOf(35.50).setScale(2), "EUR", LocalDateTime.of(2020, 3, 26, 14, 49, 7), "user1"));
        expectedData.add(new PriceEntity(2l, expectedBrand, LocalDateTime.of(2020, 6, 14, 15, 0, 0)
                , LocalDateTime.of(2020, 6, 14, 18, 30, 0), expectedProduct, 1,
                BigDecimal.valueOf(25.45).setScale(2), "EUR", LocalDateTime.of(2020, 5, 26, 15, 38, 22), "user1"));
        expectedData.add(new PriceEntity(3l, expectedBrand, LocalDateTime.of(2020, 6, 15, 0, 0, 0)
                , LocalDateTime.of(2020, 6, 15, 11, 0, 0), expectedProduct, 1,
                BigDecimal.valueOf(30.50).setScale(2), "EUR", LocalDateTime.of(2020, 5, 26, 15, 39, 22), "user2"));
        expectedData.add(new PriceEntity(4l, expectedBrand, LocalDateTime.of(2020, 6, 15, 16, 0, 0)
                , LocalDateTime.of(2020, 12, 31, 23, 59, 59), expectedProduct, 1,
                BigDecimal.valueOf(38.95).setScale(2), "EUR", LocalDateTime.of(2020, 6, 2, 10, 14, 0), "user1"));

        assertThat(priceList).containsExactlyInAnyOrderElementsOf(expectedData);

        List<BrandEntity> brandAfterExecution = brandRepository.findAll();
        List<ProductEntity> productAfterExecution = productRepository.findAll();

        assertThat(brandAfterExecution).size().isEqualTo(1);
        assertThat(brandBeforeExecution).size().isEqualTo(1);
        assertThat(brandAfterExecution.get(0).getId()).isEqualTo(brandBeforeExecution.get(0).getId());
        assertThat(brandAfterExecution.get(0).getName()).isEqualTo(brandBeforeExecution.get(0).getName());
        assertThat(productAfterExecution).size().isEqualTo(1);
        assertThat(productBeforeExecution).size().isEqualTo(1);
        assertThat(productAfterExecution.get(0).getId()).isEqualTo(productBeforeExecution.get(0).getId());
        assertThat(productAfterExecution.get(0).getName()).isEqualTo(productBeforeExecution.get(0).getName());

    }


    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db/init_database.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db/init_prices_job.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:db/clean.sql")
    })
    public void dataIsAddedToDataBaseWithData() throws Exception {
        dataIsAddedToDataBase();
    }


}