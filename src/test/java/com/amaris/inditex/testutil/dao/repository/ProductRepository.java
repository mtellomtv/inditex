package com.amaris.inditex.testutil.dao.repository;

import com.amaris.inditex.dao.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
}
