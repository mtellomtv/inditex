package com.amaris.inditex.testutil.dao.repository;

import com.amaris.inditex.dao.entity.BrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<BrandEntity, Long> {
}
