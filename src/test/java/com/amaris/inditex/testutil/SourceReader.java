package com.amaris.inditex.testutil;


import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

public class SourceReader {

    public static String getResourceAsString(String path) throws IOException, URISyntaxException {
        return IOUtils.toString(SourceReader.class.getClassLoader().getResource(path).toURI(), StandardCharsets.UTF_8);
    }

}
