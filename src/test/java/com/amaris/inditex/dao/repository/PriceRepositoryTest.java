package com.amaris.inditex.dao.repository;

import com.amaris.inditex.dao.entity.PriceEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PriceRepositoryTest {

    @Autowired
    private PriceRepository priceRepository;

    @Test
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:db/init_database.sql", "classpath:db/init_prices.sql"})
    void getFilteredData() {
        LocalDateTime applicationDate = LocalDateTime.of(2020, 6, 14, 16, 0, 0);
        List<PriceEntity> prices = priceRepository.findFilteredPrice(35455l, 1l, applicationDate);

        assertThat(prices).size().isEqualTo(2);
        assertThat(prices.get(0).getId()).isEqualTo(2);
        assertThat(prices.get(1).getId()).isEqualTo(1);
    }

}
