package com.amaris.inditex.dao;

import com.amaris.inditex.dao.entity.PriceEntity;
import com.amaris.inditex.dao.repository.PriceRepository;
import com.amaris.inditex.model.Brand;
import com.amaris.inditex.model.Price;
import com.amaris.inditex.model.Product;
import com.amaris.inditex.model.Rate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PriceH2DaoImplTest {

    @InjectMocks
    PriceH2DaoImpl priceH2Dao;

    @Mock
    PriceRepository repository;

    @Spy
    ModelMapper mapper = new ModelMapper();

    @Test
    void getCurrentPrice() {

        List<Price> prices = new ArrayList<>();
        Price price1 = Price.builder()
                .brand(Brand.builder().id(1l).name("ZARA").build())
                .currency("EUR")
                .id(3l)
                .product(Product.builder().id(1234l).name("product1").build())
                .priority(1)
                .build();
        prices.add(price1);
        Price price2 = Price.builder()
                .brand(Brand.builder().id(1l).name("ZARA").build())
                .currency("EUR")
                .id(5l)
                .product(Product.builder().id(1134l).name("product1").build())
                .priority(0)
                .build();
        prices.add(price2);

        List<PriceEntity> pricesEntity = new ArrayList<>();
        PriceEntity priceEntity1 = mapper.map(price1, PriceEntity.class);
        pricesEntity.add(priceEntity1);
        PriceEntity priceEntity2 = mapper.map(price2, PriceEntity.class);
        pricesEntity.add(priceEntity2);

        LocalDateTime now = LocalDateTime.now();
        Rate rate = Rate.builder()
                .brandId(1l)
                .productId(1234l)
                .applicationDate(now)
                .build();

        when(repository.findFilteredPrice(1234l, 1l, now)).thenReturn(pricesEntity);

        List<Price> priceReturned = priceH2Dao.getCurrentPrice(rate);

        assertThat(priceReturned).isEqualTo(prices);

    }

    @Test
    void getEmptyCurrentPrice() {

        List<Price> prices = new ArrayList<>();

        List<PriceEntity> pricesEntity = new ArrayList<>();

        LocalDateTime now = LocalDateTime.now();
        Rate rate = Rate.builder()
                .brandId(1l)
                .productId(1234l)
                .applicationDate(now)
                .build();

        when(repository.findFilteredPrice(1234l, 1l, now)).thenReturn(pricesEntity);

        List<Price> priceReturned = priceH2Dao.getCurrentPrice(rate);

        assertThat(priceReturned).isEqualTo(prices);

    }
}