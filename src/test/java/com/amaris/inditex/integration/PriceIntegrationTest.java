package com.amaris.inditex.integration;

import com.amaris.inditex.dao.entity.PriceEntity;
import com.amaris.inditex.dao.repository.PriceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.amaris.inditex.testutil.SourceReader.getResourceAsString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PriceIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PriceRepository priceRepository;

    @ParameterizedTest
    @CsvSource({"2020-06-14-10.00.00, integration/prices/filtered-data-response-one-result.json",
            "2020-06-14-15.00.00, integration/prices/filtered-data-response-better-of-two-results.json",
            "2020-06-14-16.00.00, integration/prices/filtered-data-response-better-of-two-results.json",
            "2020-06-14-21.00.00, integration/prices/filtered-data-response-2020-06-14-21.00.00.json",
            "2020-06-15-10.00.00, integration/prices/filtered-data-response-2020-06-15-10.00.00.json",
            "2020-06-16-21.00.00, integration/prices/filtered-data-response-2020-06-16-21.00.00.json"})
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:db/init_database.sql", "classpath:db/init_prices.sql"}),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:db/clean.sql")
    })
    void getFilteredData(String date, String result) throws Exception {

        List<PriceEntity> priceEntitiesBeforeCall = priceRepository.findAll();

        mockMvc.perform(get("/prices?brandId=1&productId=35455&applicationDate=" + date))
                .andExpect(status().isOk())
                .andExpect(content().json(getResourceAsString(result)));

        List<PriceEntity> priceEntitiesAfterCall = priceRepository.findAll();
        assertThat(priceEntitiesBeforeCall).containsExactlyInAnyOrderElementsOf(priceEntitiesAfterCall);
    }

    @Test
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:db/init_database.sql", "classpath:db/init_prices.sql"}),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:db/clean.sql")
    })
    void noResultFilteredData() throws Exception {

        List<PriceEntity> priceEntitiesBeforeCall = priceRepository.findAll();

        mockMvc.perform(get("/prices?brandId=1&productId=35455&applicationDate=2020-06-13-10.00.00"))
                .andExpect(status().isNotFound());

        List<PriceEntity> priceEntitiesAfterCall = priceRepository.findAll();
        assertThat(priceEntitiesBeforeCall).containsExactlyInAnyOrderElementsOf(priceEntitiesAfterCall);
    }

    @ParameterizedTest
    @CsvSource({" , 35455, 2020-06-14-16.00.00",
            "1,  , 2020-06-14-16.00.00",
            "1, 35455, "})
    @SqlGroup({
            @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:db/init_database.sql"),
            @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:db/clean.sql")
    })
    void badRequestFilteredData(String brandId, String productId, String date) throws Exception {

        List<PriceEntity> priceEntitiesBeforeCall = priceRepository.findAll();

        mockMvc.perform(get("/prices?brandId=" + brandId + "&productId=" + productId + "&applicationDate=" + date))
                .andExpect(status().isBadRequest());

        List<PriceEntity> priceEntitiesAfterCall = priceRepository.findAll();
        assertThat(priceEntitiesBeforeCall).containsExactlyInAnyOrderElementsOf(priceEntitiesAfterCall);
    }

}
