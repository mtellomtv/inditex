package com.amaris.inditex.exception.handler;

import com.amaris.inditex.exception.NotFoundPriceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class RestControllerExceptionHandler {

    @ExceptionHandler(value = {BindException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String validationException(BindException ex) {
        log.error("Malformed Requst: {}", ex.getMessage(), ex);
        return "Malformed Requst";
    }

    @ExceptionHandler(value = {NotFoundPriceException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String notfoundPriceException(NotFoundPriceException ex) {
        log.error("Rate not Found: ", ex);
        return "Rate not Found";
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public String unknownException(Exception ex) {
        log.error("Internal server error: {}", ex.getMessage(), ex);
        return "Internal server error";
    }

}
