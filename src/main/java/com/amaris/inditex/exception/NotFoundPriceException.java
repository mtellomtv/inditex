package com.amaris.inditex.exception;

import lombok.Getter;

@Getter
public class NotFoundPriceException extends RuntimeException {

    public NotFoundPriceException() {
        super();
    }
}
