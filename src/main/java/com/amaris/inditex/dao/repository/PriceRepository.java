package com.amaris.inditex.dao.repository;

import com.amaris.inditex.dao.entity.PriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface PriceRepository extends JpaRepository<PriceEntity, Long> {

    @Query("select p from PriceEntity p where p.product.id = :productId and p.brand.id = :brandId" +
            " and p.startDate <= :applicationDate and p.endDate >= :applicationDate order by p.priority desc")
    List<PriceEntity> findFilteredPrice(Long productId, Long brandId, LocalDateTime applicationDate);
}
