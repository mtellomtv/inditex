package com.amaris.inditex.dao;

import com.amaris.inditex.dao.entity.PriceEntity;
import com.amaris.inditex.dao.repository.PriceRepository;
import com.amaris.inditex.model.Price;
import com.amaris.inditex.model.Rate;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
public class PriceH2DaoImpl implements PriceDao {

    private final PriceRepository priceRepository;
    private final ModelMapper mapper;

    public List<Price> getCurrentPrice(Rate rate) {
        List<PriceEntity> priceEntities = priceRepository.findFilteredPrice(rate.getProductId(), rate.getBrandId(), rate.getApplicationDate());
        return transformPriceEntityToPriceModel(priceEntities);
    }

    private List<Price> transformPriceEntityToPriceModel(List<PriceEntity> priceEntities) {
        return priceEntities.stream().map(priceEntity -> mapper.map(priceEntity, Price.class)).collect(Collectors.toList());
    }
}
