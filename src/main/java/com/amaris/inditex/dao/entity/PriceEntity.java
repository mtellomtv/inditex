package com.amaris.inditex.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "PRICES")
public class PriceEntity {

    @Id
    @Column(name = "PRICE_LIST")
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "BRAND_ID")
    private BrandEntity brand;

    @Column(name = "START_DATE")
    private LocalDateTime startDate;

    @Column(name = "END_DATE")
    private LocalDateTime endDate;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "PRODUCT_ID")
    private ProductEntity product;

    @Column(name = "PRIORITY")
    private Integer priority;

    @Column(name = "PRICE")
    private BigDecimal priceValue;

    @Column(name = "CURR")
    private String currency;

    @Column(name = "LAST_UPDATE")
    private LocalDateTime lastUpdate;

    @Column(name = "LAST_UPDATE_BY")
    private String lastUpdateBy;

    @Override
    public String toString() {
        return "PriceEntity{" +
                "id=" + id +
                ", brand=" + brand.getId() +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", product=" + product.getId() +
                ", priority=" + priority +
                ", price=" + priceValue +
                ", currency='" + currency +
                ", lastUpdate=" + lastUpdate +
                ", lastUpdateBy=" + lastUpdateBy + '\'' +
                '}';
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PriceEntity)) return false;
        final PriceEntity other = (PriceEntity) o;
        if (!other.canEqual((Object) this)) return false;
        final Object thisId = this.getId();
        final Object otherId = other.getId();
        if (thisId == null ? otherId != null : !thisId.equals(otherId)) return false;
        final Object thisBrand = this.getBrand();
        final Object otherBrand = other.getBrand();
        if (thisBrand == null ? otherBrand != null : !equalsBrand(thisBrand, otherBrand)) return false;
        final Object thisStartDate = this.getStartDate();
        final Object otherStartDate = other.getStartDate();
        if (thisStartDate == null ? otherStartDate != null : !thisStartDate.equals(otherStartDate)) return false;
        final Object thisEndDate = this.getEndDate();
        final Object otherEndDate = other.getEndDate();
        if (thisEndDate == null ? otherEndDate != null : !thisEndDate.equals(otherEndDate)) return false;
        final Object thisProduct = this.getProduct();
        final Object otherProduct = other.getProduct();
        if (thisProduct == null ? otherProduct != null : !equalsProduct(thisProduct, otherProduct)) return false;
        final Object thisPriority = this.getPriority();
        final Object otherPriority = other.getPriority();
        if (thisPriority == null ? otherPriority != null : !thisPriority.equals(otherPriority)) return false;
        final Object thisPrice = this.getPriceValue();
        final Object otherPrice = other.getPriceValue();
        if (thisPrice == null ? otherPrice != null : !thisPrice.equals(otherPrice)) return false;
        final Object thisCurrency = this.getCurrency();
        final Object otherCurrency = other.getCurrency();
        if (thisCurrency == null ? otherCurrency != null : !thisCurrency.equals(otherCurrency)) return false;
        final Object thisLastUpdate = this.getLastUpdate();
        final Object otherLastUpdate = other.getLastUpdate();
        if (thisLastUpdate == null ? otherLastUpdate != null : !thisLastUpdate.equals(otherLastUpdate)) return false;
        final Object thisLastUpdateBy = this.getLastUpdateBy();
        final Object otherLastUpdateBy = other.getLastUpdateBy();
        if (thisLastUpdateBy == null ? otherLastUpdateBy != null : !thisLastUpdateBy.equals(otherLastUpdateBy))
            return false;
        return true;
    }

    private boolean equalsBrand(Object thisBrand, Object otherBrand) {
        return ((BrandEntity) thisBrand).getId() == null ? ((BrandEntity) otherBrand).getId() != null : ((BrandEntity) thisBrand).getId().equals(((BrandEntity) otherBrand).getId());
    }

    private boolean equalsProduct(Object thisProduct, Object otherProduct) {
        return ((ProductEntity) thisProduct).getId() == null ? ((ProductEntity) otherProduct).getId() != null : ((ProductEntity) thisProduct).getId().equals(((ProductEntity) otherProduct).getId());
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PriceEntity;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object thisId = this.getId();
        result = result * PRIME + (thisId == null ? 43 : thisId.hashCode());
        final Object thisBrand = this.getBrand();
        result = result * PRIME + (thisBrand == null ? 43 : thisBrand.hashCode());
        final Object thisStartDate = this.getStartDate();
        result = result * PRIME + (thisStartDate == null ? 43 : thisStartDate.hashCode());
        final Object thisEndDate = this.getEndDate();
        result = result * PRIME + (thisEndDate == null ? 43 : thisEndDate.hashCode());
        final Object thisProduct = this.getProduct();
        result = result * PRIME + (thisProduct == null ? 43 : thisProduct.hashCode());
        final Object thisPriority = this.getPriority();
        result = result * PRIME + (thisPriority == null ? 43 : thisPriority.hashCode());
        final Object thisPrice = this.getPriceValue();
        result = result * PRIME + (thisPrice == null ? 43 : thisPrice.hashCode());
        final Object thisCurrency = this.getCurrency();
        result = result * PRIME + (thisCurrency == null ? 43 : thisCurrency.hashCode());
        final Object thisLastUpdate = this.getLastUpdate();
        result = result * PRIME + (thisLastUpdate == null ? 43 : thisLastUpdate.hashCode());
        final Object thisLastUpdateBy = this.getLastUpdateBy();
        result = result * PRIME + (thisLastUpdateBy == null ? 43 : thisLastUpdateBy.hashCode());
        return result;
    }
}
