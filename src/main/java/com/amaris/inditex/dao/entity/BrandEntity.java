package com.amaris.inditex.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "BRANDS")
public class BrandEntity {

    @Id
    private Long id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "brand")
    private List<PriceEntity> prices;

}
