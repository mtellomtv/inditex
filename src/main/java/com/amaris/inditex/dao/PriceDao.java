package com.amaris.inditex.dao;

import com.amaris.inditex.model.Price;
import com.amaris.inditex.model.Rate;

import java.util.List;

public interface PriceDao {
    List<Price> getCurrentPrice(Rate rate);
}
