package com.amaris.inditex.batch.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:job/csv.properties")
@ConfigurationProperties(prefix = "csv")
@Getter
@Setter
@Configuration
public class CSVProperties {
    private String source;
}
