package com.amaris.inditex.batch.job;

import com.amaris.inditex.batch.mapper.PriceFieldSetMapper;
import com.amaris.inditex.batch.model.PriceBatchData;
import com.amaris.inditex.batch.properties.CSVProperties;
import com.amaris.inditex.batch.task.DeletePriceDataBaseTask;
import com.amaris.inditex.dao.entity.BrandEntity;
import com.amaris.inditex.dao.entity.PriceEntity;
import com.amaris.inditex.dao.entity.ProductEntity;
import com.amaris.inditex.dao.repository.PriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.persistence.EntityManagerFactory;

@RequiredArgsConstructor
@EnableBatchProcessing
@EnableScheduling
@Configuration
public class PriceCSVToDataBase {

    private final EntityManagerFactory entityManagerFactory;
    private final CSVProperties csvProperties;

    @Bean
    public Job jobPriceCSVToDataBase(JobBuilderFactory jobBuilderFactory,
                                     @Qualifier("cleanTable") Step cleanTable,
                                     @Qualifier("step1") Step step1) {
        return jobBuilderFactory.get("job")
                .start(cleanTable)
                .next(step1)
                .build();
    }

    @Bean
    public Step cleanTable(StepBuilderFactory stepBuilderFactory, DeletePriceDataBaseTask fileDeletingTasklet) {
        return stepBuilderFactory.get("step2")
                .tasklet(fileDeletingTasklet)
                .build();
    }

    @Bean
    public Step step1(StepBuilderFactory stepBuilderFactory) {
        return stepBuilderFactory.get("stepSavePriceOfCSVInDataBase")
                .<PriceBatchData, PriceEntity>chunk(10)
                .reader(priceInCSVItemReader())
                .processor(priceInCSVToDBProcessor())
                .writer(priceToDBVItemWriter())
                .build();
    }

    @Bean
    public DeletePriceDataBaseTask fileDeletingTasklet(PriceRepository priceRepository) {
        DeletePriceDataBaseTask tasklet = new DeletePriceDataBaseTask(priceRepository);
        return tasklet;
    }

    public FlatFileItemReader<PriceBatchData> priceInCSVItemReader() {
        FlatFileItemReader<PriceBatchData> reader = new FlatFileItemReader<>();
        reader.setLinesToSkip(1);
        reader.setResource(new ClassPathResource(csvProperties.getSource()));

        DefaultLineMapper<PriceBatchData> priceMapper = new DefaultLineMapper<>();

        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
        tokenizer.setDelimiter(",");
        tokenizer.setNames(new String[]{"brandId", "startDate", "endDate", "priceList", "productId", "priority", "priceValue",
                "currency", "lastUpdate", "lastUpdateBy"});

        priceMapper.setLineTokenizer(tokenizer);
        priceMapper.setFieldSetMapper(new PriceFieldSetMapper());
        priceMapper.afterPropertiesSet();
        reader.setLineMapper(priceMapper);
        return reader;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public JpaItemWriter<PriceEntity> priceToDBVItemWriter() {
        JpaItemWriter<PriceEntity> jpaItemWriter = new JpaItemWriter<>();
        jpaItemWriter.setEntityManagerFactory(entityManagerFactory);
        return jpaItemWriter;
    }

    public ItemProcessor<PriceBatchData, PriceEntity> priceInCSVToDBProcessor() {

        return price -> PriceEntity.builder()
                .id(price.getId())
                .brand(BrandEntity.builder()
                        .id(price.getBrandId())
                        .build())
                .startDate(price.getStartDate())
                .endDate(price.getEndDate())
                .product(ProductEntity.builder()
                        .id(price.getProductId())
                        .build())
                .priority(price.getPriority())
                .priceValue(price.getPriceValue())
                .currency(price.getCurrency())
                .lastUpdate(price.getLastUpdate())
                .lastUpdateBy(price.getLastUpdateBy())
                .build();
    }
}