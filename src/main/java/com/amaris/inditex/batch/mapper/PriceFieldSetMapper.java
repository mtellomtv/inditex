package com.amaris.inditex.batch.mapper;

import com.amaris.inditex.batch.model.PriceBatchData;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import java.time.ZoneId;

public class PriceFieldSetMapper implements FieldSetMapper<PriceBatchData> {

    @Override
    public PriceBatchData mapFieldSet(FieldSet fieldSet) {
        return PriceBatchData.builder()
                .id(fieldSet.readLong("priceList"))
                .brandId(fieldSet.readLong("brandId"))
                .startDate(fieldSet.readDate("startDate", "yyyy-MM-dd-HH.mm.ss").toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime())
                .endDate(fieldSet.readDate("endDate", "yyyy-MM-dd-HH.mm.ss").toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime())
                .productId(fieldSet.readLong("productId"))
                .priority(fieldSet.readInt("priority"))
                .priceValue(fieldSet.readBigDecimal("priceValue"))
                .currency(fieldSet.readString("currency"))
                .lastUpdate(fieldSet.readDate("lastUpdate", "yyyy-MM-dd-HH.mm.ss").toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime())
                .lastUpdateBy(fieldSet.readString("lastUpdateBy"))
                .build();
    }
}