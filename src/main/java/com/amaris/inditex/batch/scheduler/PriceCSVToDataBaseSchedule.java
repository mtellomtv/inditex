package com.amaris.inditex.batch.scheduler;

import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@AllArgsConstructor
@EnableScheduling
public class PriceCSVToDataBaseSchedule {

    private final JobLauncher jobLauncher;

    @Qualifier("jobPriceCSVToDataBase")
    private final Job jobPriceCSVToDataBase;

    @Scheduled(cron = "0 0 0 * * ?")
    public void scheduleTaskPriceCSVToDataBase() throws Exception {
        JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncher.run(jobPriceCSVToDataBase, params);
    }
}
