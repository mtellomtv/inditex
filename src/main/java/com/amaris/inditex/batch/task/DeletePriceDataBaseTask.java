package com.amaris.inditex.batch.task;

import com.amaris.inditex.dao.repository.PriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

@RequiredArgsConstructor
public class DeletePriceDataBaseTask implements Tasklet {

    private final PriceRepository priceRepository;

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

        priceRepository.deleteAll();

        return RepeatStatus.FINISHED;
    }
}