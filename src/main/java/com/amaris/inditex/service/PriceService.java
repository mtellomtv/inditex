package com.amaris.inditex.service;

import com.amaris.inditex.model.Price;
import com.amaris.inditex.model.Rate;

public interface PriceService {
    Price getCurrentPrice(Rate rate);
}
