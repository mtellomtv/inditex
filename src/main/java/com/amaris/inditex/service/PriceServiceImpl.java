package com.amaris.inditex.service;

import com.amaris.inditex.dao.PriceDao;
import com.amaris.inditex.exception.NotFoundPriceException;
import com.amaris.inditex.model.Price;
import com.amaris.inditex.model.Rate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
@Service
public class PriceServiceImpl implements PriceService {

    private final PriceDao priceDao;

    @Override
    public Price getCurrentPrice(Rate rate) {
        List<Price> prices = priceDao.getCurrentPrice(rate);
        return ofNullable(prices)
                .map(List::stream)
                .orElseThrow(NotFoundPriceException::new)
                .findFirst()
                .orElseThrow(NotFoundPriceException::new);
    }
}