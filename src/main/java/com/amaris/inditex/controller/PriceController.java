package com.amaris.inditex.controller;

import com.amaris.inditex.controller.dto.RateRequestDto;
import com.amaris.inditex.controller.dto.RateResponseDto;
import com.amaris.inditex.model.Price;
import com.amaris.inditex.model.Rate;
import com.amaris.inditex.service.PriceService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("prices")
public class PriceController {

    private final PriceService priceService;
    private final ModelMapper mapper;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public RateResponseDto getCurrentPrice(RateRequestDto rateRequestDto) {
        Rate rate = mapper.map(rateRequestDto, Rate.class);
        Price price = priceService.getCurrentPrice(rate);
        return mapper.map(price, RateResponseDto.class);
    }
}
