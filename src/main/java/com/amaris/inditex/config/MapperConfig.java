package com.amaris.inditex.config;

import com.amaris.inditex.controller.dto.RateResponseDto;
import com.amaris.inditex.model.Price;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        setAutoMapperConfig(modelMapper);
        return modelMapper;
    }

    private void setAutoMapperConfig(ModelMapper modelMapper) {
        modelMapper.createTypeMap(Price.class, RateResponseDto.class)
                .addMapping(Price::getId, RateResponseDto::setRateId)
                .addMapping(Price::getPriceValue, RateResponseDto::setPrice);

    }
}
